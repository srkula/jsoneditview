import React from 'react';
import JsonItems from './components/JsonItems/JsonItems';
import { data } from './dummyData';
const App = () => {

    return (
        <div>
            <JsonItems items={data} />
        </div>
    );
}

export default App;