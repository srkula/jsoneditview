import React, { useState } from 'react';
import './JsonItem.css';

const JsonItem = (props) => {
    const [id, setId] = useState(props.id);
    const [isActive, setIsActive] = useState(props.isActive);
    const [picture, setPicture] = useState(props.picture);
    const [age, setAge] = useState(props.age);
    const [name, setName] = useState(props.name);
    const [email, setEmail] = useState(props.email);
    const [phone, setPhone] = useState(props.phone);
    const [address, setAddress] = useState(props.address);
    const [about, setAbout] = useState(props.about);
    const [registered, setRegistered] = useState(props.registered);

    console.log('JsonItem');
    const setIdHandler = (event) => {
        setId(event.target.value);
    };
    const setIsActiveHandler = (event) => {
        if (event.target.value === 'true') {
            setIsActive(false);
        } else {
            setIsActive(true);
        }
    };
    const setPictureHandler = (event) => {
        setPicture(event.target.value);
    };
    const setAgeHandler = (event) => {
        setAge(event.target.value);
    };
    const setNameHandler = (event) => {
        setName(event.target.value);
    };
    const setEmailHandler = (event) => {
        setEmail(event.target.value);
    };
    const setPhoneHandler = (event) => {
        setPhone(event.target.value);
    };
    const setAddressHandler = (event) => {
        setAddress(event.target.value);
    };
    const setAboutHandler = (event) => {
        setAbout(event.target.value);
    };
    const setRegisteredHandler = (event) => {
        var hasValue = registered ? true : false;
        let dateTime = new Date(event.target.value).toISOString();
        setRegistered(dateTime, hasValue);
    };

    return (
        <>
            <div className='items-controls-list'>
                <div className="items-with-control">
                    <p>"id":"{id}"</p>
                    <div className="control">
                        {!id ?
                            <input
                                type="text"
                                onBlur={setIdHandler}
                                placeholder="id"
                            />
                            : ''}
                    </div>
                </div>
                <div className="items-with-control">
                    <p>"isActive":"{isActive + ''}"</p>
                    <div className="control">
                        <input type="checkbox" id="isActive" name="isActive" onChange={setIsActiveHandler} checked={isActive} value={isActive} />
                        <label htmlFor="isActive">Active</label>
                    </div>
                </div>
                <div className="items-with-control">
                    <p>"picture":"{picture}"</p>
                    <div className="control">
                        <input
                            type="text"
                            onChange={setPictureHandler}
                            placeholder="picture"
                        />
                    </div>
                </div>
                <div className="items-with-control ageBlock">
                    <p>"age":"{age}"</p>
                    <div className="control">
                        <input
                            type="number"
                            min="1"
                            step="1"
                            onChange={setAgeHandler}
                            placeholder="age"
                        />
                    </div>
                </div>
                <div className="items-with-control">
                    <p>"name":"{name}"</p>
                    <div className="control">
                        <input
                            type="text"
                            onChange={setNameHandler}
                            placeholder="name"
                        />
                    </div>
                </div>
                <div className="items-with-control">
                    <p>"email":"{email}"</p>
                    <div className="control">
                        <input
                            type="email"
                            onChange={setEmailHandler}
                            placeholder="email"
                        />
                    </div>
                </div>
                <div className="items-with-control">
                    <p>"phone":"{phone}"</p>
                    <div className="control">
                        <input
                            type="text"
                            onChange={setPhoneHandler}
                            placeholder="phone"
                        />
                    </div>
                </div>
                <div className="items-with-control">
                    <p>"address":"{address}"</p>
                    <div className="control">
                        <input
                            type="text"
                            onChange={setAddressHandler}
                            placeholder="address"
                        />
                    </div>
                </div>
                <div className="items-with-control aboutSection">
                    <p className="aboutText">"about":"{about}"</p>
                    <div className="control">
                        <textarea
                            type="text"
                            onChange={setAboutHandler}
                            placeholder="about"
                            cols="20"
                            rows="2"
                        />
                    </div>
                </div>
            </div>
        </>
    );
}

export default JsonItem;