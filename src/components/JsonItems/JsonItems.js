import React from 'react';
import JsonItem from './JsonItem';
import './JsonItems.css';

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

const JsonItems = (props) => {
    return (
        <>
            {props.items.map(item =>
                <JsonItem key={uuidv4()}
                    name={item.name}
                    age={item.age}
                    isActive={item.isActive}
                    id={item._id}
                    picture={item.picture}
                    email={item.email}
                    phone={item.phone}
                    address={item.address}
                    about={item.about}
                    registered={item.registered}
                />
            )}
        </>
    );
}

export default JsonItems;